package com.crespro.specialnames;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread splashscreen = new Thread() {
            @Override
            public void run() {
                try{
                    sleep(1000);

                }catch (InterruptedException e){
                    e.printStackTrace();

                }finally {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);

                }


            }
        }; splashscreen.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
