package com.crespro.specialnames.helper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by USER2015 on 3/2/2016.
 */
public interface APIClient {

    @Headers("api_key: ISLAMICNAMES2016")
    @FormUrlEncoded
    @POST("names") //tengok hujung URL
    //parameter
    Call<ResponseBody> getNames(@Field("name") String name,
                                @Field("rows") int rows,
                                @Field("start") int start);


    @Headers("api_key: ISLAMICNAMES2016")
    @FormUrlEncoded
    @POST("addname") //tengok hujung URL
    Call<ResponseBody> addName(@Field("name") String name,
                               @Field("gender") String gender,
                               @Field("arabic") String arabic,
                               @Field("en_us") String en_us,
                               @Field("ms_my") String my_ms );

}


