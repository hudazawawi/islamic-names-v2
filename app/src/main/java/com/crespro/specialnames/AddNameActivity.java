package com.crespro.specialnames;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.crespro.specialnames.helper.APIClient;
import com.crespro.specialnames.helper.ServiceGenerator;
import com.crespro.specialnames.model.Name;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNameActivity extends AppCompatActivity  {

    EditText etName, etEnglish, etMalay, etArabic;
    Spinner spinGender;

    Name name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_name);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initEl();
    }

    private void initEl() {

        // get reference to the views
        etName = (EditText) findViewById(R.id.etName);
        etEnglish = (EditText) findViewById(R.id.etEnglish);
        etMalay = (EditText) findViewById(R.id.etMalay);
        etArabic = (EditText) findViewById(R.id.etArabic);
        spinGender = (Spinner) findViewById(R.id.spinGender);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.Gender, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGender.setAdapter(adapter);


    }

    public void onClickAdd(View view) {
        String gender = null;
        if (spinGender.getSelectedItemPosition() == 0){
            gender = "male";
        }
        else if (spinGender.getSelectedItemPosition() == 1){
            gender = "female";
        }

        Log.e ("jihuhk","hbjjj");
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.addName(etName.getText().toString(),
                gender,
                etArabic.getText().toString(),
                etEnglish.getText().toString(),
                etMalay.getText().toString());
        result.enqueue(new Callback<ResponseBody>() {

            public void onResponse(Response<ResponseBody> response) {
                try {
                    if (response.isSuccess()) {
                        Log.e("Success", "asad");
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        String status = jsonObject.getString("status");
                        Toast.makeText(AddNameActivity.this, status, Toast.LENGTH_SHORT).show();


                    }else{
                        Log.e("NO", "asad");
                        JSONObject jsonObject = new JSONObject(new String(response.errorBody().string()));
                        Toast.makeText(AddNameActivity.this, "Sorry, already exist", Toast.LENGTH_SHORT).show();
                    }


                }catch (JSONException e) {
                    e.printStackTrace();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(AddNameActivity.this, "Sorry", Toast.LENGTH_SHORT).show();
            }
        });



    }

}