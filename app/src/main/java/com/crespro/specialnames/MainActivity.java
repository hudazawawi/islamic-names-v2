package com.crespro.specialnames;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crespro.specialnames.adapter.NameAdapter;
import com.crespro.specialnames.helper.APIClient;
import com.crespro.specialnames.helper.ServiceGenerator;
import com.crespro.specialnames.model.Name;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AbsListView.OnScrollListener, OnItemClickListener, TextView.OnEditorActionListener{


    EditText etCari;
    NameAdapter nameAdapter;
    ListView lvNama;
    ImageView ivGender;
    ProgressDialog progressDialog;

    int start = 0;
    int total = 0;
    boolean newSearch = true;
    boolean isLoading = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); //hide keyboard when open app

        initElements();
        loadDataFromServer();

    }



    private void initElements()
    {
        etCari = (EditText) findViewById(R.id.etCari);
        etCari.setOnEditorActionListener(this);
        ivGender = (ImageView) findViewById(R.id.ivGender);
        lvNama = (ListView) findViewById(R.id.lvNama);

        nameAdapter = new NameAdapter(this);
        lvNama.setAdapter(nameAdapter);
        lvNama.setOnScrollListener(this);
        lvNama.setOnItemClickListener(this);

    }

    public void onClickAdd(View view) {
        Intent intent = new Intent(MainActivity.this, AddNameActivity.class);
        startActivity(intent);
    }

    public void onClickFavorite(View view) {
        Intent intent = new Intent(MainActivity.this, FavoriteActivity.class);
        startActivity(intent);
    }



    public void onClickSearch(View view) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        start = 0;
        newSearch = true;
        nameAdapter.getNameArrayList().clear();
        loadDataFromServer();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        //ignore this
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (totalItemCount != total) {
            if (totalItemCount - (firstVisibleItem + visibleItemCount) < 5) {
                start = totalItemCount;
                if (!isLoading) {
                    newSearch = false;
                    loadDataFromServer();
                }
            }
        }
    }

    private void loadDataFromServer() {
        if (newSearch) {
            progressDialog = ProgressDialog.show(this, null, "loading....", true);
        }
        isLoading = true;
        APIClient client = ServiceGenerator.createService(APIClient.class);
        Call<ResponseBody> result = client.getNames(etCari.getText().toString(), 20, start);
        result.enqueue(new Callback<ResponseBody>() {

            public void onResponse(Response<ResponseBody> response) {

                if (response.isSuccess()) {

                    try {
                        JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                        JSONObject schemaJSONObject = jsonObject.getJSONObject("schema");
                        total = schemaJSONObject.getInt("total");
                        Log.d("aaaa", jsonObject.toString());

                        JSONArray jsonArray = jsonObject.getJSONArray("contact");    //read data array bernama contact from JSON
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject contactObj = jsonArray.getJSONObject(i); //read data obj from dari jsonArray

                            Name name = new Name(contactObj.getString("name"),
                                    contactObj.getString("gender"),
                                    contactObj.getString("en_us"),
                                    contactObj.getString("ms_my"),
                                    contactObj.getString("id")
//                                    contactObj.getString("arabic")
                            );
                            nameAdapter.getNameArrayList().add(name);  //adapter dapatkan arraylist drpd class adapter pastu add
                        }
                        nameAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                isLoading = false;
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {

                isLoading = false;
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) { //button search in keyboard
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            Log.e("key", v.getText().toString());
            start = 0;
            newSearch = true;
            nameAdapter.getNameArrayList().clear();
            loadDataFromServer();
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Name name = nameAdapter.getNameArrayList().get(position);
        String text = name.getName() +
                "\n" + "en: " + name.getEnglish()
                + "\n" + "my: " + name.getMalay();
//                + "\n" + "ar: " + name.getArabic();

        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clipData = android.content.ClipData.newPlainText("Clip", text);
        Toast.makeText(getApplicationContext(), "Copied to Clipboard", Toast.LENGTH_SHORT).show();
        clipboard.setPrimaryClip(clipData);
    }



}
