package com.crespro.specialnames;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.crespro.specialnames.adapter.FavoriteAdapter;
import com.crespro.specialnames.model.Favorite;

public class FavoriteActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView lvFavorite;
    FavoriteAdapter favoriteAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        initEl();

    }

    private void initEl() {
        lvFavorite = (ListView) findViewById(R.id.lvFavorite);
//        ivGender = (ImageView) findViewById(R.id.ivGender);

        favoriteAdapter = new FavoriteAdapter(this);
        lvFavorite.setAdapter(favoriteAdapter);
        lvFavorite.setOnItemClickListener(FavoriteActivity.this);

    }




    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Favorite favorite = favoriteAdapter.getFavoriteArrayList().get(position);
        String text = favorite.getName() +
                "\n" + "en: " + favorite.getEnglish()
                + "\n" + "my: " + favorite.getMalay();

        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clipData = android.content.ClipData.newPlainText("Clip", text);
        Toast.makeText(getApplicationContext(), "Copied to Clipboard", Toast.LENGTH_SHORT).show();
        clipboard.setPrimaryClip(clipData);
    }


}