package com.crespro.specialnames.model;

import com.orm.SugarRecord;

/**
 * Created by USER2015 on 3/2/2016.
 */
public class Name extends SugarRecord {

    String name, gender, english, malay, nameid;
    private boolean selected;

    public Name(){
    }


    public Name(String name, String gender, String english, String malay, String nameid) {
        this.name = name;
        this.gender = gender;
        this.english = english;
        this.malay = malay;
        this.nameid = nameid;
    }


    public String getNameid() {
        return nameid;
    }

    public void setNameid(String nameid) {
        this.nameid = nameid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getMalay() {
        return malay;
    }

    public void setMalay(String malay) {
        this.malay = malay;
    }


    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

}
