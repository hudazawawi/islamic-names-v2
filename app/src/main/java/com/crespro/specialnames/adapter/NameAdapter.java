package com.crespro.specialnames.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crespro.specialnames.R;
import com.crespro.specialnames.model.Favorite;
import com.crespro.specialnames.model.Name;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER2015 on 3/2/2016.
 */
public class NameAdapter extends BaseAdapter {


    Context context;
    ArrayList<Name> nameArrayList;



    public NameAdapter(Context context) {
        this.context = context;
        nameArrayList = new ArrayList<>();

    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<Name> getNameArrayList() {
        return nameArrayList;
    }

    public void setNameArrayList(ArrayList<Name> nameArrayList) {
        this.nameArrayList = nameArrayList;
    }

    @Override
    public int getCount() {
        return nameArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return nameArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_name, null);

        // Get TextView
        TextView tvNama = (TextView) convertView.findViewById(R.id.tvNama);
        TextView tvEnglish = (TextView) convertView.findViewById(R.id.tvEnglish);
        TextView tvMalay = (TextView) convertView.findViewById(R.id.tvMalay);
        final CheckBox cbFavorite = (CheckBox) convertView.findViewById(R.id.cbFavorite);

        final Name name = (Name) getItem(position);

        //check content -favorite ada @ xdok
        List<Favorite> favList = Favorite.find(Favorite.class, "nameid = ?", name.getNameid());
        if (favList.size() > 0) {
            cbFavorite.setChecked(true);
        }
        else {
            cbFavorite.setChecked(false);
        }

        cbFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            // Check if it is favorite or not
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {

                    Favorite fav = new Favorite(nameArrayList.get(position).getName(),
                            nameArrayList.get(position).getGender(),
                            nameArrayList.get(position).getEnglish(),
                            nameArrayList.get(position).getMalay(),
                            nameArrayList.get(position).getNameid()
                    );
                    fav.save();

                    Toast.makeText(context.getApplicationContext(), "Add to Favorite", Toast.LENGTH_LONG).show();

                    Log.e("set", "true");
                } else {

                    List<Favorite> favList = Favorite.find(Favorite.class, "nameid = ?", name.getNameid());
                    if (favList.size() > 0) {
                        Favorite fav = favList.get(0);
                        fav.delete();
                    }

                    Toast.makeText(context.getApplicationContext(), "Remove from Favorite", Toast.LENGTH_LONG).show();
                    Log.e("set", "false");
                }

            }
        });



        tvNama.setText(name.getName());
        tvEnglish.setText(name.getEnglish());
        tvMalay.setText(name.getMalay());

        // Get ImageView
        ImageView ivGender = (ImageView) convertView.findViewById(R.id.ivGender);

        // Check if it is male or female
        // Set the image and colour of text name
        if (name.getGender().equals("F")) {
            ivGender.setImageResource(R.drawable.girl);
//            tvNama.setTextColor(Color.parseColor("#e91e63"));

        } else if (name.getGender().equals("M")) {
            ivGender.setImageResource(R.drawable.boy);
//            tvNama.setTextColor(Color.parseColor("#2196f3"));

        }


        return convertView;
    }

    @Override
    public void notifyDataSetInvalidated() {

        nameArrayList = (ArrayList<Name>) Name.listAll(Name.class);
        super.notifyDataSetInvalidated();
    }


}
