package com.crespro.specialnames.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.crespro.specialnames.R;
import com.crespro.specialnames.model.Favorite;

import java.util.List;

/**
 * Created by USER2015 on 3/22/2016.
 */
public class FavoriteAdapter extends BaseAdapter {

    Context context;
    List<Favorite> favoriteArrayList;



    public FavoriteAdapter(Context context) {
        this.context = context;
        favoriteArrayList = Favorite.listAll(Favorite.class);

        Log.e("size", String.valueOf(favoriteArrayList.size()));
    }

    @Override
    public int getCount() {
        return favoriteArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return favoriteArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Favorite> getFavoriteArrayList() {
        return favoriteArrayList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_fav, null);

        final Favorite fav = (Favorite) getItem(position);

        // Get TextView
        TextView tvNama = (TextView) convertView.findViewById(R.id.tvNama);
        tvNama.setText(favoriteArrayList.get(position).getName());

        TextView tvEnglish = (TextView) convertView.findViewById(R.id.tvEnglish);
        tvEnglish.setText(favoriteArrayList.get(position).getEnglish());

        TextView tvMalay = (TextView) convertView.findViewById(R.id.tvMalay);
        tvMalay.setText(favoriteArrayList.get(position).getMalay());

        final CheckBox cbFavorite = (CheckBox) convertView.findViewById(R.id.cbFavorite);
        cbFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            // Check if it is favorite or not
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {


                } else {

                    final List<Favorite> favList = Favorite.find(Favorite.class, "nameid = ?", fav.getNameid());
                    if (favList.size() > 0) {


                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setMessage("Are you sure want to delete?");

                        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                Favorite fav = favList.get(0);
                                fav.delete();

                                favoriteArrayList.remove(position);
                                notifyDataSetChanged();

                            }
                        });

                        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }

                            private void finish() {

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                    }

                }

            }

        });


        // Get ImageView
        ImageView ivGender = (ImageView) convertView.findViewById(R.id.ivGender);

        // Check if it is male or female
        // Set the image and colour of text name
        if (fav.getGender().equals("F")){
            ivGender.setImageResource(R.drawable.girl);
        }

        else if (fav.getGender().equals("M")){
            ivGender.setImageResource(R.drawable.boy);
        }

        return convertView;
    }


}
